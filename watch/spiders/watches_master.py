# -*- coding: utf-8 -*-
import scrapy
import pymongo
import re
from slugify import slugify

from watch.items import *

clean_str = lambda str: re.sub("^\s+|\n|\r|\s+$", '', str)

def get_prop_key(name):
	values = {
		u'tip':'style',
		u'material_korpusa':'housing_material',
		u'razmer_korpusa':'case_size',
		u'pokrytie_korpusa': 'coating',
		u'tolshchina_korpusa':'case_thickness',
		u'forma_korpusa':'case_shape',
		u'steklo':'glass',
		u'tip_kalibra':'type_of_fire',
		u'kalibr':'caliber',
		u'zapas_khoda':'power_reserve',
		u'funktsii':'options',
		u'braslet':'bangle',
		u'tsvet_remeshka':'band_color',
		u'zastiozhka':'clasp',
		u'vodonepronitsaemost':'water_resistant',
		u'tsena_rekomenduemaia_proizvoditelem':'retail',
		u'sostoianie':'condition',
		u'tsvet_tsiferblata':'dial_color',
		u'tip_khronografa':'type_chronograph',
		u'alternativnyi_referens_1':'alt_ref1',
		u'alternativnyi_referens_2':'alt_ref2',
		u'alternativnyi_referens_3':'alt_ref3',
		u'alternativnyi_referens_4':'alt_ref4',
		u'garantiia': 'garantiia',
		u'vypusk_chasov': 'vypusk_chasov',
		u'est_v_nalichii': 'est_v_nalichii',
		u'piktogrammki': 'piktogrammki',
		u'kolichestvo_ekzempliarov': 'kolichestvo_ekzempliarov',
		u'nashi_rekomendatsii': 'nashi_rekomendatsii'

	}
	return values[name]

############################################################################################

class BrandsSpider(scrapy.Spider):
	name = "brandes-spider"
	allowed_domains = ["watches-master.com"]
	start_urls = (
		'http://www.watches-master.com/',
	)

	#парсер для брендов
	def parse(self, response):
		for href in response.css(".brandes>ul>div>li>a::attr('href')"):
			brand_url = response.urljoin(href.extract())
			yield scrapy.Request(brand_url, callback=self.parse_brand)

	def parse_brand(self, response):
		for href in response.css("span.set>figcaption>div>a::attr('href')"):
			collection_url = response.urljoin(href.extract())
			yield WatchCollection(url=collection_url, parsed=False, TYPE="collection")
		yield WatchBrand(url=response.url, parsed=True, TYPE="brand")

############################################################################################

class CollectionsSpider(scrapy.Spider):
	name = "collections-spider"
	allowed_domains = ["watches-master.com"]
	
	def start_requests(self):
		client = pymongo.MongoClient()
		self.c = client['watch']['collection_1']
		for collection in self.c.find({'TYPE':'collection', 'parsed' : False}):
			yield scrapy.Request(collection['url'], self.parse)


	def parse(self, response):
		for href in response.css("figcaption>div.item_name>a::attr('href')"):
			watch_url = response.urljoin(href.extract())
			if self.c.find_one({'url':watch_url}) == None:
				yield scrapy.Request(watch_url, callback=self.parse_watch)
		yield WatchCollection(url=response.url, parsed=True, TYPE="collection")

	def parse_watch(self, response):
		watch = WatchItem(url=response.url, parsed=True, TYPE="watch")
		
		#main info
		watch['name']  		= response.xpath("//*[@id='zaraza']/div[2]/div[1]/h1/span/span[1]/text()").extract_first()
		watch['brand'] 		= response.xpath("//*[@id='zaraza']/div/div/div/h2/span/text()").extract_first()
		watch['collection'] = response.xpath("/html/body/div[6]/ul/li[3]/a/span/text()").extract_first()
		watch['alias'] 		= response.xpath("//*[@id='zaraza']/div/div/div/h3/text()").extract_first()
		watch['ref'] 		= response.xpath("//*[@id='zaraza']/div[2]/div[1]/span/text()").extract_first()
		#main info end

		##images 
		watch['image_0'] = response.xpath("//*[@id='zaraza']/div[2]/div[1]/center/a/img/@src").extract_first()
		
		image_num = 1
		for image_src in response.xpath("//*[@id='zaraza']/div[2]/div[1]/div[1]/a/img/@src"):
			watch['image_'+str(image_num)]=image_src.extract()
			image_num+=1
		##images end

		#extended info
		for line in response.xpath("//*[@itemprop='mentions']"):
			prop_name = line.xpath("td[1]/strong/text()").extract_first()
			
			key = slugify(unicode(prop_name), word_boundary=True, save_order=True, separator="_")
			if key == 'tip_kalibra':
				prop_value = line.xpath("td[2]/a/text()").extract_first()
			else:
				prop_value = " ".join(line.xpath("td[2]/span/text()").extract())
				if prop_value == "":
					prop_value = clean_str(unicode(" ".join(line.xpath("td[2]/text()").extract())))
			
			if prop_value == "":
				prop_value = " ".join(line.xpath("td[2]/strong/text()").extract())
			
			if prop_value == "":
				prop_value = " ".join(line.xpath("td[2]").extract())
			

			watch[get_prop_key(key)] = prop_value
		#extended info end

		#same models
		same_models = []
		for ref in response.xpath("//*[@id='zaraza']/div[4]/div/div[2]/div/figure/div/figcaption/div/strong/text()"):
			same_models.append(ref.extract())
		watch['same_models'] = "|".join(same_models)
		#same models end

		yield watch

############################################################################################

def get_c():
	cl = pymongo.MongoClient()
	c = cl['watch']['collection_1']
	return c

class SearchSpider(scrapy.Spider):
	name = "search-spider"
	allowed_domains = ["watches-master.com"]
	start_urls = (
		'http://watches-master.com/watches/search/page1?reference=',
		'http://watches-master.com/watches/search/page104?reference=',
	)

	c = get_c()

	def parse(self, response):
		for href in response.css("figcaption>div.item_name>a::attr('href')"):
			watch_url = response.urljoin(href.extract())
			if self.c.find_one({'url':watch_url}) == None:
				yield scrapy.Request(watch_url, callback=self.parse_watch)

		href = response.css("#yw0 > li.next > a::attr('href')").extract_first()
		if href != u'javascript:void(0)':
			next_url = response.urljoin(href)
			yield scrapy.Request(next_url, callback=self.parse)
	
	def parse_watch(self, response):
		watch = WatchItem(url=response.url, parsed=True, TYPE="watch")
		
		#main info
		watch['name']  		= response.xpath("//*[@id='zaraza']/div[2]/div[1]/h1/span/span[1]/text()").extract_first()
		watch['brand'] 		= response.xpath("//*[@id='zaraza']/div/div/div/h2/span/text()").extract_first()
		watch['collection'] = response.xpath("/html/body/div[6]/ul/li[3]/a/span/text()").extract_first()
		watch['alias'] 		= response.xpath("//*[@id='zaraza']/div/div/div/h3/text()").extract_first()
		watch['ref'] 		= response.xpath("//*[@id='zaraza']/div[2]/div[1]/span/text()").extract_first()
		#main info end

		##images 
		watch['image_0'] = response.xpath("//*[@id='zaraza']/div[2]/div[1]/center/a/@href").extract_first()
		
		image_num = 1
		for image_src in response.xpath("//*[@id='zaraza']/div[2]/div[1]/div[1]/a/@href"):
			watch['image_'+str(image_num)]=image_src.extract()
			image_num+=1
		##images end

		#extended info
		for line in response.xpath("//*[@itemprop='mentions']"):
			prop_name = line.xpath("td[1]/strong/text()").extract_first()
			
			key = slugify(unicode(prop_name), word_boundary=True, save_order=True, separator="_")
			if key == 'tip_kalibra':
				prop_value = line.xpath("td[2]/a/text()").extract_first()
			else:
				prop_value = " ".join(line.xpath("td[2]/span/text()").extract())
				if prop_value == "":
					prop_value = clean_str(unicode(" ".join(line.xpath("td[2]/text()").extract())))
			
			if prop_value == "":
				prop_value = " ".join(line.xpath("td[2]/strong/text()").extract())
			
			if prop_value == "":
				prop_value = " ".join(line.xpath("td[2]").extract())
			

			watch[get_prop_key(key)] = prop_value
		#extended info end

		#same models
		same_models = []
		for ref in response.xpath("//*[@id='zaraza']/div[4]/div/div[2]/div/figure/div/figcaption/div/strong/text()"):
			same_models.append(ref.extract())
		watch['same_models'] = "|".join(same_models)
		#same models end

		yield watch


class ImageSpider(scrapy.Spider):
	name = "img-spider"
	allowed_domains = ["watches-master.com"]
	start_urls = (
		'http://watches-master.com/watches/search/page1?reference=',
		'http://watches-master.com/watches/search/page104?reference=',
	)

	c = get_c()

	def parse(self, response):
		for href in response.css("figcaption>div.item_name>a::attr('href')"):
			watch_url = response.urljoin(href.extract())
			if self.c.find_one({'url':watch_url}) == None:
				yield scrapy.Request(watch_url, callback=self.parse_watch)

		href = response.css("#yw0 > li.next > a::attr('href')").extract_first()
		if href != u'javascript:void(0)':
			next_url = response.urljoin(href)
			yield scrapy.Request(next_url, callback=self.parse)
	
	def parse_watch(self, response):
		watch = WatchImg(url=response.url, parsed=True, TYPE="WatchImg")

		#image_0 
		watch['image_0'] = response.xpath("//*[@id='zaraza']/div[2]/div[1]/center/a/@href").extract_first()
		
		yield watch


