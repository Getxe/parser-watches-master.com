# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class WatchBrand(scrapy.Item):
	TYPE = scrapy.Field()
	url = scrapy.Field()
	parsed = scrapy.Field()

class WatchImg(scrapy.Item):
	TYPE = scrapy.Field()
	parsed = scrapy.Field()
	url = scrapy.Field()
	image_0 = scrapy.Field()

class WatchCollection(scrapy.Item):
	TYPE = scrapy.Field()
	url = scrapy.Field()
	parsed = scrapy.Field()

class WatchItem(scrapy.Item):
	TYPE = scrapy.Field()
	url = scrapy.Field()
	parsed = scrapy.Field()
	#main info
	brand = scrapy.Field()#Производитель
	collection = scrapy.Field()#Коллекция
	name = scrapy.Field()#Название
	alias = scrapy.Field()#Также известен как
	ref = scrapy.Field()#Референс

	#extended info
	style = scrapy.Field()#Тип
	housing_material = scrapy.Field()#Материал корпуса
	coating = scrapy.Field()#покрытие корпуса
	case_size = scrapy.Field()#Размер корпуса
	case_thickness = scrapy.Field()#Толщина корпуса
	case_shape = scrapy.Field()#Форма корпуса

	glass = scrapy.Field()#Стекло

	type_of_fire = scrapy.Field()#Тип калибра
	caliber = scrapy.Field() #Калибр

	power_reserve = scrapy.Field() #Запас хода

	options = scrapy.Field()#Функции
	bangle = scrapy.Field()#Браслет
	band_color = scrapy.Field()#Цвет ремешка

	clasp = scrapy.Field()#Застёжка
	water_resistant = scrapy.Field()#Водонепроницаемость
	retail = scrapy.Field()#Цена, рекомендуемая производителем

	condition = scrapy.Field()#Состояние
	dial_color = scrapy.Field()#Цвет циферблата

	type_chronograph = scrapy.Field()#Тип хронографа

	alt_ref1 = scrapy.Field()#Альтернативный референс 1
	alt_ref2 = scrapy.Field()#Альтернативный референс 2
	alt_ref3 = scrapy.Field()#Альтернативный референс 3
	alt_ref4 = scrapy.Field()#Альтернативный референс 4

	garantiia = scrapy.Field()#Гарантия
	vypusk_chasov = scrapy.Field()#Выпуск часов
	est_v_nalichii = scrapy.Field()#Выпуск часов
	piktogrammki = scrapy.Field()#Выпуск часов
	kolichestvo_ekzempliarov = scrapy.Field()#
	same_models = scrapy.Field()#ПОХОЖИЕ МОДЕЛИ

	image_0		= scrapy.Field()#Изображение 0	
	image_1		= scrapy.Field()#Изображение 1	
	image_2		= scrapy.Field()#Изображение 2	
	image_3		= scrapy.Field()#Изображение 3	
	image_4		= scrapy.Field()#Изображение 4	
	image_5		= scrapy.Field()#Изображение 5	
	image_6		= scrapy.Field()#Изображение 6	
	image_7		= scrapy.Field()#Изображение 7	
	image_8		= scrapy.Field()#Изображение 8	
	image_9		= scrapy.Field()#Изображение 9	
	image_10	= scrapy.Field()#Изображение 10
	image_11	= scrapy.Field()#Изображение 11
	image_12	= scrapy.Field()#Изображение 12
	image_13	= scrapy.Field()#Изображение 13
	image_14	= scrapy.Field()#Изображение 14
	image_15	= scrapy.Field()#Изображение 15
	image_16	= scrapy.Field()#Изображение 16
	image_17	= scrapy.Field()#Изображение 17
	image_18	= scrapy.Field()#Изображение 18
	image_19	= scrapy.Field()#Изображение 19
	image_20	= scrapy.Field()#Изображение 20
	image_21	= scrapy.Field()#Изображение 21
	image_22	= scrapy.Field()#Изображение 22
	image_23	= scrapy.Field()#Изображение 23
	image_24	= scrapy.Field()#Изображение 24
	image_25	= scrapy.Field()#Изображение 25
	image_26	= scrapy.Field()#Изображение
	image_27	= scrapy.Field()#Изображение
	image_28	= scrapy.Field()#Изображение
	image_29	= scrapy.Field()#Изображение
	image_30	= scrapy.Field()#Изображение
	image_31	= scrapy.Field()#Изображение
	image_32	= scrapy.Field()#Изображение
	image_33	= scrapy.Field()#Изображение
	image_34	= scrapy.Field()#Изображение
	image_35	= scrapy.Field()#Изображение
	image_36	= scrapy.Field()#Изображение
	image_37	= scrapy.Field()#Изображение
	image_38	= scrapy.Field()#Изображение
	image_39	= scrapy.Field()#Изображение
	image_40	= scrapy.Field()#Изображение
	
	nashi_rekomendatsii	= scrapy.Field()#nashi_rekomendatsii

