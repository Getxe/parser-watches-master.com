# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo

class AbstractMongoPipeline(object):

	def __init__(self, mongo_uri, mongo_db):
		self.mongo_uri = mongo_uri
		self.mongo_db = mongo_db

	def run_with(self, spider):
		return self.pipeline_name in getattr(spider, 'pipelines', [])

	def get_collection(self):
		return self.db[self.collection_name]

	@classmethod
	def from_crawler(cls, crawler):
		return cls(
			mongo_uri=crawler.settings.get('MONGO_URI'),
			mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
		)

	def open_spider(self, spider):
		self.client = pymongo.MongoClient(self.mongo_uri)
		self.db = self.client[self.mongo_db]

	def close_spider(self, spider):
		self.client.close()

class MainMongoPipeLine(AbstractMongoPipeline):
	
	pipeline_name = 'main_pipe'
	collection_name = 'collection_1'

	def process_item(self, item, spider):
		if item['TYPE'] == 'brand':
			self.get_collection().insert_one(dict(item))
		if item['TYPE'] == 'collection':
			if item['parsed'] == False:
				self.get_collection().insert_one(dict(item))
			else:
				item_filter = {"url": item['url'], "TYPE":item['TYPE']}
				self.get_collection().find_one_and_update(item_filter, { '$set': { 'parsed' : item['parsed'] } })
		if item['TYPE'] == 'watch':
			if self.get_collection().find_one({'url':item['url']}) == None:
				self.get_collection().insert_one(dict(item))

		if item['TYPE'] == 'WatchImg':
			if self.get_collection().find_one({'url':item['url'], 'TYPE':'WatchImg'}) == None:
				self.get_collection().insert_one(dict(item))

		return item