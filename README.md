# Установка: #
1. установить MongoDB ([https://www.mongodb.org/downloads](https://www.mongodb.org/downloads))
2. установить зависимости: 

```
#!console

pip install -r requirements.txt
easy_install pymongo
```

# Использование: #
Запустить сервер MongoDB из папки проекта: (db.conf - конфиг в папке проекта) 
```
#!console
F:\MongoDB\bin\mongod -f db.conf 4.1. 
```

Парсинг: 

```
#!console
scrapy crawl search-spider
```

Выгрузить в CSV из MongoDB:

```
#!console

F:\MongoDB\bin\mongoexport -d watch -c collection_1 -q "{ 'TYPE' : 'watch'}" --fieldFile fields.txt --type csv --out "watches.csv"

```